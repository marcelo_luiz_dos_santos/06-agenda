package br.com.itau;

import java.util.*;

public class Agenda {
    private Contato contato;

    public Agenda(Contato contato) {
        this.contato = contato;
    }

    public Agenda() {
    }

    private Map<String, String> agenda = new HashMap<>();
    Scanner scanner = new Scanner(System.in);

    public void verificaInclusao(){

        System.out.print("Quantos contatos você deseja incluir na lista: ");
        int quantidadeDeContatos = scanner.nextInt();

        for (int i = 1; i <= quantidadeDeContatos; i++){
            Contato contato = new Contato();

            System.out.print("Digite o e-mail " + i + ": ");
            contato.setEmail(scanner.next());

            System.out.print("Digite o telefone " + i + ": ");
            contato.setTelefone(scanner.next());

            this.inserir(contato.getEmail(), contato.getTelefone());
        }
        this.consultarlista();
    }

    public void verificaMaisAcao(){
        System.out.print("O que deseja fazer? (I = Inserir | E = Excluir | S = Sair): ");
        String acao = scanner.next();
        switch(acao){
            case "i":
            case "I":
                this.verificaInclusao();
                this.verificaMaisAcao();
                break;
            case "e":
            case "E":
                System.out.print("Como deseja excluir? (1 = E-mail | 2 = Telefone): ");
                String modoExclusao = scanner.next();
                switch (modoExclusao){
                    case "1":
                        Contato contato = new Contato();

                        System.out.print("Digite o e-mail para exclusão: ");
                        contato.setEmail(scanner.next());

                        this.deletarPorEmail(contato.getEmail());
                        this.consultarlista();
                        this.verificaMaisAcao();
                        break;
                    case "2":
                        Contato contato2 = new Contato();

                        System.out.print("Digite o telefone para exclusão: ");
                        contato2.setTelefone(scanner.next());

                        this.deletarPorTelefone(contato2.getTelefone());
                        this.consultarlista();
                        this.verificaMaisAcao();
                        break;
                    default:
                        System.out.println("");
                        System.out.println("!!! Opção inválida !!!");
                        System.out.println("");
                        this.verificaMaisAcao();
                        break;
                }
                break;
            case "s":
            case "S":
                System.out.println("");
                System.out.println("***** Muito obrigado *****");
                break;
            default:
                System.out.println("");
                System.out.println("!!! Opção inválida !!!");
                System.out.println("");
                this.verificaMaisAcao();
                break;
        }
    }

    public void consultarlista() {
        System.out.println("");
        System.out.println("**********  CONTATOS NA AGENDA  **********");
        for(Map.Entry<String, String> entry : agenda.entrySet()){
            System.out.println("Email: " + entry.getKey() + " - " + "Telefone: " + entry.getValue());
        }
        System.out.println("");
    }

    public void inserir(String email, String telefone){
        agenda.put(email, telefone);
    }

    public void deletarPorEmail(String email){
        agenda.remove(email);
    }

    public void deletarPorTelefone(String telefone) {
        String chave = "";
        for (Map.Entry<String, String> me : agenda.entrySet()) {
            if (me.getValue().equals(telefone)) {
                chave = me.getKey();
            }
        }
        if(!chave.equals("")) {
            agenda.remove(chave);
        }
    }
}